module.exports = {
  "extends": "standard",
  "plugins": [
    "html"
  ],
  "settings": {
    "html/indent": "+2"  // indentation is the <script> indentation plus two spaces.
  }
};